0) mesa limpia y parados en el path de la app
kubectl get pods
kubectl get svs

* Podemos crear desde los pods, como desventaja al actualizar la app esta va a estar indisponible y tendremos que hacerlo manual:

1) crear voting-app y check 
kubectl create -f voting-app-pod.yaml
kubectl create -f voting-app-service.yaml
kubectl get pods, svc # para check todo junto
minikube service voting-service --url # me dará la url para acceder via web (check que esta levantada app)

2) crear redis y check
kubectl create -f redis-pod.yaml
kubectl create -f redis-service.yaml
kubectl get pods, svc

3) crear postgres db y check
kubectl create -f postgres-pod.yaml
kubectl create -f postgres-service.yaml
kubectl get pods, svc

4) crear worker y check
kubectl create -f worker-pod.yaml
kubectl get pods, svc

5) crear result-app y check 
kubectl create -f result-app-pod.yaml
kubectl create -f result-app-service.yaml
kubectl get pods, svc # para check todo junto
minikube service result-service --url # me dará la url para acceder via web (check que esta levantada app)

** otra forma es desde los deploy que se encargaran de genera los replicaset para escalar cuando sea necesario actualizar.

1) crear voting-app y check 
kubectl create -f voting-app-deploy.yaml
kubectl create -f voting-app-service.yaml
kubectl get deployments, svc # para check todo junto
minikube service voting-service --url # me dará la url para acceder via web (check que esta levantada app)

2) crear redis y check
kubectl create -f redis-deploy.yaml
kubectl create -f redis-service.yaml
kubectl get deployments, svc

3) crear postgres db y check
kubectl create -f postgres-deploy.yaml
kubectl create -f postgres-service.yaml
kubectl get deployments, svc

4) crear worker y check
kubectl create -f worker-deploy.yaml
kubectl get deployments, svc

5) crear result-app y check 
kubectl create -f result-app-deploy.yaml
kubectl create -f result-app-service.yaml
kubectl get deployments, svc # para check todo junto
minikube service result-service --url # me dará la url para acceder via web (check que esta levantada app)

*** fin ***